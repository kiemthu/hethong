var vm;

var map;
var distance;
var directionsService;
var directionsDisplay;

var suser='';
var scar;

var orderRef;

var isPicking = false;
var marker, mcar;
var curOrser;

vm = new Vue({
    el: '#app',
    data: {
        currentOrder: '',
        orders: []
    },
    filters: {
        showTime: function (value) {
            if (!value) return null;
            var d = new Date(value);
            return d.toLocaleString();
        }
    },
    methods: {
        selectOrder: function (key) {
            var self = this;
            if (key == self.currentOrder)
                return;
            if (!isPicking) {
                self.currentOrder = key;
            }
        },
        startPick: function () {
            if (!isPicking) {
                var self = this;
                $.each(self.orders, function (idx, value) {
                    if (value.key == self.currentOrder) {
                        var itref = firebase.database().ref('Order/' + self.currentOrder);
                        itref.transaction(function (currentData) {
                            console.log(currentData);
                            if (currentData.CarID == null || currentData.CarID == '') {
                                currentData.Status = 3;
                                currentData.CarID = suser;
                                console.log(currentData);
                                isPicking = true;
                                return currentData;
                            } else {
                                return; // Abort the transaction.
                            }
                        }, function (error, committed, snapshot) {
                            if (error) {
                                console.log('Transaction failed abnormally!', error);
                            } else if (!committed) {
                                console.log('We aborted the transaction.');
                                isPicking = false;
                                $.each(vm.orders, function (idxx, valuex) {
                                    if (valuex.key == self.currentOrder) {
                                        vm.orders.splice(idxx, 1);
                                        return false;
                                    }
                                });
                            } else {
                                console.log('Set');
                                firebase.database().ref('/Car/' + suser).update({ hasPicked: true });
                                curOrser = snapshot.val();
                                showinfo();
                            }
                            return false;
                        }, false);
                    }
                });
            }
        },
        endPick: function () {
            var self = this;
            if (isPicking) {
                isPicking = false;
                $.each(self.orders, function (idx, value) {
                    if (value.key == self.currentOrder) {
                        var updates = {};
                        updates['/' + self.currentOrder + '/Status'] = 5;
                        orderRef.update(updates);

                        firebase.database().ref('/Car/' + suser).update({ hasPicked: false });
                        curOrser = null;

                        hideInfo();
                        return false;
                    }
                });
            }
        }
    }
});

function showinfo() {
    if (isPicking) {

        hideInfo();

        directionsService.route({
            origin: scar.LatLng,
            destination: curOrser.LatLng,
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                marker = new google.maps.Marker({
                    map: map,
                    position: curOrser.LatLng
                });
                mcar = new google.maps.Marker({
                    map: map,
                    position: scar.LatLng,
                    icon: "http://maps.google.com/mapfiles/kml/pal4/icon62.png"
                });
            }
        });
    }
}
function hideInfo() {
    if (marker != null) {
        marker.setMap(null);
        marker = null;
    }
    if (mcar != null) {
        mcar.setMap(null);
        mcar = null;
    }
    directionsDisplay.setDirections({ routes: [] });
}

$('#btnLogin').on('click', function () {
    if (!firebase.auth().currentUser) {
        var email = "user@nvb.us";
        var spass = $('#txtPass').val();
        suser = $('#txtUser').val();
        $('#txtPass').val('');
        $('#txtUser').val('');

        //login
        firebase.auth().signInWithEmailAndPassword(email, "123456")
            .then(function (user) {
                return firebase.database().ref('/Car/' + suser).once('value').then((snap) => {
                    if (snap.val() != null && snap.val().Password == spass) {
                        if (snap.val().isLogin) {
                            alert("Lái xe đã đang nhập");
                            firebase.auth().signOut();
                            $('#modalLogin').modal('show');
                        } else {
                            scar = snap.val();
                            firebase.database().ref('/Car/' + suser).update({ isLogin: true });
                            loadOrders();
                            $('#modalLogin').modal('hide');
                        }
                    } else {
                        alert("Mã Lái xe hoặc mật khẩu không chính xác");
                        firebase.auth().signOut();
                        $('#modalLogin').modal('show');
                    }
                });
            })
            .catch(function (error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode === 'auth/wrong-password') {
                    alert('Mật khẩu sai.');
                } else {
                    alert(errorMessage);
                }
                console.log(error);
            });
    }
});

$('#btnLogout').on('click', function () {
    if (firebase.auth().currentUser) {

        if (isPicking) {
            var updates = {};
            updates['/' + vm.currentOrder + '/Status'] = 2;
            updates['/' + vm.currentOrder + '/CarID'] = '';
            orderRef.update(updates);
        }

        if (suser != '') {
            firebase.database().ref('/Car/' + suser).update({ isLogin: false, hasPicked: false });
        }
        firebase.auth().signOut();
    }
});

firebase.auth().onAuthStateChanged(function (user) {
    if (user != null) {
        if (suser == '') {
            $('#modalLogin').modal('show');
            firebase.auth().signOut();
        }
    } else {
        $('#modalLogin').modal('show');
    }
});

function loadOrders() {
    orderRef = firebase.database().ref('Order');
    orderRef.off();

    vm.orders = [];

    var getOrder = function (data) {
        var vdata = data.val();
        if (vdata.IsVIP != scar.IsVIP)
            return;
        if (vdata.Status == 2) {
            distance.getDistanceMatrix({
                origins: [scar.LatLng],
                destinations: [vdata.LatLng],
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (response, status) {
                if (status !== 'OK') {
                    alert('Error was: ' + status);
                    return;
                }
                if (response.rows[0].elements[0].distance.value < 6000) {
                    vm.orders.unshift({ key: data.key, info: vdata });
                }
            });
        } else if ((vdata.Status == 3 && (!isPicking || vm.currentOrder != data.key)) || vdata.Status == 5 || vdata.Status == 4) {
            $.each(vm.orders, function (idx, value) {
                if (value.key == data.key) {
                    vm.orders.splice(idx, 1);
                    return false;
                }
            });
        }
    }
    orderRef.limitToLast(10).on("child_added", getOrder);
    orderRef.limitToLast(10).on("child_changed", getOrder);
}


function initMap() {
    var hcmus = { lat: 10.762695566012058, lng: 106.68256759643555 };
    distance = new google.maps.DistanceMatrixService;

    directionsService = new google.maps.DirectionsService;

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: hcmus,
        clickableIcons: false
    });
    var rendererOptions = {
        map: map,
        suppressMarkers: true,
        polylineOptions: {
            strokeColor: "#759fdd",
            strokeWeight: 7
        }
    }
    directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
    directionsDisplay.setPanel(document.getElementById('waypanel'));

    map.addListener('click', function (e) {
        if (isPicking) {
            scar.LatLng.lat = e.latLng.lat();
            scar.LatLng.lng = e.latLng.lng();
            firebase.database().ref('/Car/' + suser).update({ LatLng: scar.LatLng });
            showinfo();
        }
    });
};

window.onbeforeunload = function (event) {

    if (isPicking) {
        var updates = {};
        updates['/' + vm.currentOrder + '/Status'] = 2;
        updates['/' + vm.currentOrder + '/CarID'] = '';
        orderRef.update(updates);
    }

    if (suser != '') {
        firebase.database().ref('/Car/' + suser).update({ isLogin: false, hasPicked: false });
    }
    firebase.auth().signOut();
    event.returnValue = "No exit";
};