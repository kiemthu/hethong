//GLOBAR VAR
var orderRef;
var vm;

// app vuejs
vm = new Vue({
    el: '#appDTV',
    data: {
        orders: [],
        newOrderNumber: '',
        newOrderAddress: '',
        newOrderIsVIP: false,
        newOrderLatLng: [],
        newOrderStatus: 0,
        status: ["Chưa định vị", "Đang định vị", "Đang tìm xe", "Đã có xe nhận", "Không có xe nhận", "Hoàn thành chuyến đi"],
        hasPicked: false
    },
    filters: {
        showTime: function (value) {
            if (!value) return null;
            var d = new Date(value);
            return d.toLocaleString();
        }
    },
    methods: {
        onlyNumber: function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                evt.preventDefault();
                return;
            }
            return false;
        },
        findByNumber: function () {
            var self = this;
            self.orders = [];
            self.hasPicked = false;
            self.newOrderStatus = 0;
            self.newOrderLatLng = [];
            self.newOrderAddress = '';
            if (self.newOrderNumber.length >= 10 && self.newOrderNumber.length < 13) {
                loadOrders();
            }
        },
        changeAddress: function () {
            var self = this;
            self.hasPicked = false;
            self.newOrderStatus = 0;
            self.newOrderLatLng = [];
        },
        pickFromHistory: function (key) {
            console.log(key);
            var self = this;
            $.each(self.orders, function (idx, value) {
                if (value.key == key) {
                    if (value.info.Status == 5) {
                        self.hasPicked = true;
                        self.newOrderAddress = value.info.Address;
                        self.newOrderLatLng = value.info.LatLng;
                        self.newOrderStatus = 2;
                    } else {
                        self.hasPicked = false;
                        self.newOrderStatus = 0;
                        self.newOrderLatLng = [];
                        self.newOrderAddress = '';
                    }
                    return false;
                }
            });
        },
        sendNewOrder: function () {
            sendOrder();
        }
    }
});

$('#btnLogin').on('click', function () {
    if (!firebase.auth().currentUser) {
        var email = "user@nvb.us";
        var pass = $('#txtPass').val();

        // login
        firebase.auth().signInWithEmailAndPassword(email, pass).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // [START_EXCLUDE]
            if (errorCode === 'auth/wrong-password') {
                alert('Mật khẩu sai.');
            } else {
                alert(errorMessage);
            }
            console.log(error);
            // [END_EXCLUDE]
        });
    }
});

$('#btnLogout').on('click', function () {

    if (firebase.auth().currentUser) {
        firebase.auth().signOut();
    }
});

firebase.auth().onAuthStateChanged(function (user) {
    vm.orders = [];
    if (user) {
        $('#modalLogin').modal('hide');
        loadOrders();
    } else {
        $('#txtPass').val('');
        $('#modalLogin').modal('show');
        vm.orders = [];
        vm.newOrderNumber = '';
        vm.newOrderAddress = '';
        vm.newOrderIsVIP = false;
        vm.newOrderLatLng = [];
        vm.newOrderStatus = 0;
        vm.hasPicked = false;
    }
});

var loadOrders = function () {
    orderRef = firebase.database().ref('Order');
    orderRef.off();

    console.log(vm.newOrderNumber);

    orderRef.orderByChild("Number").equalTo(vm.newOrderNumber).limitToLast(10).on("child_added", function (data) {
        vm.orders.push({ key: data.key, info: data.val() });
    });
    orderRef.orderByChild("Number").equalTo(vm.newOrderNumber).limitToLast(10).on("child_changed", function (data) {
        $.each(vm.orders, function (idx, value) {
            if (value.key == data.key) {
                value.info = data.val();
            }
        });
    });
};

var sendOrder = function () {

    if (vm.newOrderNumber.length < 10 ||
        vm.newOrderNumber.length > 12 ||
        vm.newOrderAddress.length < 5) {
        alert("Dữ liệu nhập không hợp lệ");
        return;
    }

    var nO = {
        Address: vm.newOrderAddress,
        IsVIP: vm.newOrderIsVIP,
        Number: vm.newOrderNumber,
        Status: vm.newOrderStatus,
        LatLng: [],
        Time: Date.now()
    };
    if (vm.hasPicked) {
        nO.LatLng = {
            lat: vm.newOrderLatLng.lat,
            lng: vm.newOrderLatLng.lng
        };
    }

    orderRef.push(nO)
        .then(function () {
            vm.orders = [];
            vm.newOrderNumber = '';
            vm.newOrderAddress = '';
            vm.newOrderIsVIP = false;
            vm.newOrderLatLng = [];
            vm.newOrderStatus = 0;
            vm.hasPicked = false;
        }).catch(function (error) {
            console.error(error);
        });
};
