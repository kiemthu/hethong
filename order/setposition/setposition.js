//GLOBAL VAR
var orderRef;
var vm;
var map;
var geocoder;
var distance;
var isPicking = false;
var marker;
var mcar = [];
var cars = [];
var cisV = false;
var noCar = false;

vm = new Vue({
    el: '#app',
    data: {
        currentOrder: '',
        orders: [],
        suser: ''
    },
    filters: {
        showTime: function (value) {
            if (!value) return null;
            var d = new Date(value);
            return d.toLocaleString();
        }
    },
    methods: {
        selectOrder: function (key) {
            var self = this;
            if (!isPicking) {
                self.currentOrder = key;
            }
        },
        startPick: function () {
            if (!isPicking) {
                var self = this;


                $.each(self.orders, function (idx, value) {
                    if (value.key == self.currentOrder) {
                        var itref = firebase.database().ref('Order/' + self.currentOrder);
                        itref.transaction(function (currentData) {
                            console.log(currentData);
                            if (currentData.Locator == null || currentData.Locator == '') {
                                currentData.Status = 1;
                                currentData.Locator = vm.suser;
                                console.log(currentData);
                                isPicking = true;
                                return currentData;
                            } else {
                                return; // Abort the transaction.
                            }
                        }, function (error, committed, snapshot) {
                            if (error) {
                                console.log('Transaction failed abnormally!', error);
                            } else if (!committed) {
                                console.log('We aborted the transaction.');
                                isPicking = false;
                                $.each(vm.orders, function (idxx, valuex) {
                                    if (valuex.key == self.currentOrder) {
                                        vm.orders.splice(idxx, 1);
                                        return false;
                                    }
                                });
                            } else {
                                console.log('Set');

                                noCar = false;

                                $('#pac-input').val(value.info.Address);
                                cisV = value.info.IsVIP;
                                codeAddress(value.info.Address);
                            }
                            console.log("data: ", snapshot.val());
                            return false;
                        }, false);
                    }
                });
            }
        },
        endPick: function () {
            var self = this;
            if (marker == null) {
                alert("Yêu cầu định vị địa chỉ đang chọn ít nhất 1 lần (click trên bản đồ)");
                return;
            }
            if (isPicking) {
                isPicking = false;
                $.each(self.orders, function (idx, value) {
                    if (value.key == self.currentOrder) {
                        var updates = {};
                        if (noCar) {
                            alert("Không có xe ở trong khu vực 5km, hủy chuyến!");
                            updates['/' + self.currentOrder + '/Status'] = 4;
                        } else {
                            updates['/' + self.currentOrder + '/Status'] = 2;
                        }
                        updates['/' + self.currentOrder + '/LatLng'] = {
                            lat: marker.position.lat(),
                            lng: marker.position.lng()
                        };

                        orderRef.update(updates);
                        $('#pac-input').val('');
                        $('#pac-output').val('');

                        marker.setMap(null);
                        marker = null;
                        mcar.forEach(function (m) {
                            m.setMap(null);
                        });
                        mcar = [];

                        return false;
                    }
                });
            }
        }
    }
});

$('#btnLogin').on('click', function () {
    if (!firebase.auth().currentUser) {
        var email = "user@nvb.us";
        var spass = $('#txtPass').val();
        var suser = $('#txtUser').val();
        $('#txtPass').val('');
        $('#txtUser').val('');
        vm.suser = suser;

        //login
        firebase.auth().signInWithEmailAndPassword(email, spass)
            .then(function (user) {
                return firebase.database().ref('/Locator/' + suser).once('value').then((snap) => {
                    if (snap.val() != null) {
                        if (snap.val().isLogin) {
                            alert("Kiểm soát viên đã đang nhập");
                            firebase.auth().signOut();
                            $('#modalLogin').modal('show');
                        } else {
                            firebase.database().ref('/Locator/' + suser).update({ isLogin: true });
                            loadOrders();
                            $('#modalLogin').modal('hide');
                        }
                    } else {
                        alert("Mã kiểm sát viên không chính xác");
                        firebase.auth().signOut();
                        $('#modalLogin').modal('show');
                    }
                });
            })
            .catch(function (error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode === 'auth/wrong-password') {
                    alert('Mật khẩu sai.');
                } else {
                    alert(errorMessage);
                }
                console.log(error);
            });
    }
});

$('#btnLogout').on('click', function () {
    if (firebase.auth().currentUser) {

        if (vm.user != '') {
            firebase.database().ref('/Locator/' + vm.suser).update({ isLogin: false });
        }

        if (isPicking) {
            isPicking = false;
            $.each(vm.orders, function (idx, value) {
                if (value.key == vm.currentOrder) {
                    var updates = {};
                    updates['/' + vm.currentOrder + '/Status'] = 0;
                    updates['/' + vm.currentOrder + '/Locator'] = '';
                    orderRef.update(updates);

                    $('#pac-input').val('');
                    $('#pac-output').val('');

                    marker.setMap(null);
                    marker = null;
                    mcar.forEach(function (m) {
                        m.setMap(null);
                    });
                    mcar = [];
                    return false;
                }
            });
        }

        firebase.auth().signOut();
    }
});

firebase.auth().onAuthStateChanged(function (user) {
    if (user != null) {
        if (vm.suser == '') {
            $('#modalLogin').modal('show');
            firebase.auth().signOut();
        }
    } else {
        $('#modalLogin').modal('show');
    }
});

var loadOrders = function () {
    orderRef = firebase.database().ref('Order');
    orderRef.off();

    vm.orders = [];

    orderRef.orderByChild("Status").equalTo(0).limitToLast(10).on("child_added", function (data) {
        vm.orders.push({ key: data.key, info: data.val() });
    });

    var rmOrder01 = function (data) {
        if (!isPicking || vm.currentOrder != data.key) {
            $.each(vm.orders, function (idx, value) {
                if (value.key == data.key) {
                    vm.orders.splice(idx, 1);
                    return false;
                }
            });
        }
    };
    orderRef.orderByChild("Status").equalTo(1).limitToLast(10).on("child_added", rmOrder01);
    orderRef.orderByChild("Status").equalTo(1).limitToLast(10).on("child_changed", rmOrder01);

    var rmOrder02 = function (data) {
        $.each(vm.orders, function (idx, value) {
            if (value.key == data.key) {
                vm.orders.splice(idx, 1);
                return false;
            }
        });
    };
    orderRef.orderByChild("Status").equalTo(2).limitToLast(10).on("child_added", rmOrder02);
    orderRef.orderByChild("Status").equalTo(2).limitToLast(10).on("child_changed", rmOrder02);

    var rmOrder03 = function (data) {
        $.each(vm.orders, function (idx, value) {
            if (value.key == data.key) {
                vm.orders.splice(idx, 1);
                return false;
            }
        });

    };
    orderRef.orderByChild("Status").equalTo(4).limitToLast(10).on("child_added", rmOrder03);
    orderRef.orderByChild("Status").equalTo(4).limitToLast(10).on("child_changed", rmOrder03);

    firebase.database().ref('/Car/').once("value").then(function (data) {
        cars = data.val();
    });
};

function codeAddress(address) {
    geocoder.geocode({ 'address': address, 'region': 'hos' }, function (results, status) {
        if (status == 'OK') {
            map.setCenter(results[0].geometry.location);
            if (marker != null) {
                marker.setMap(null);
                marker = null;
            }
            marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
            carnear();
        }
    });
};

function addressCode(code) {
    geocoder.geocode({ 'location': code }, function (results, status) {
        if (status == 'OK') {
            $('#pac-output').val(results[0].formatted_address);
        }
    });
};

function carnear() {
    findPcar(6000);

    mcar.forEach(function (m) {
        m.setMap(null);
    });
    mcar = [];


    pcar = pcar.slice(0, 25);
    noCar = pcar.length == 0;

    if (noCar)
        return;

    distance.getDistanceMatrix({
        origins: pcar,
        destinations: [marker.position],
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
    }, function (response, status) {
        if (status !== 'OK') {
            alert('Error was: ' + status);
            noCar = true;
            return;
        }
        //console.log(response);
        var n = response.rows.length;
        for (var i = 0; i < n - 1; i++) {
            for (var j = i + 1; j < n; j++) {
                if (response.rows[i].elements[0].distance.value > response.rows[j].elements[0].distance.value) {
                    var a = response.rows[i].elements[0];
                    response.rows[i].elements[0] = response.rows[j].elements[0];
                    response.rows[j].elements[0] = a;
                    var b = pcar[i];
                    pcar[i] = pcar[j];
                    pcar[j] = b;
                }
            }
        }
        console.log(response.rows);
        console.log(pcar);
        pcar = pcar.slice(0, 10);
        noCar = pcar.length == 0;

        for (var car in pcar) {
            mcar.push(new google.maps.Marker({
                map: map,
                position: { lat: pcar[car].lat, lng: pcar[car].lng },
                icon: "http://maps.google.com/mapfiles/kml/pal4/icon62.png"
            }));
        }
        pcar = [];

    });
};
var pcar = [];
var tt;
function findPcar(d) {
    var lat2 = marker.position.lat(), lng2 = marker.position.lng();
    $.each(cars, function (idx, car) {
        if (car.IsVIP != cisV)
            return;
        //var b = (car.LatLng.lat - lat2) * (car.LatLng.lat - lat2) + (car.LatLng.lng - lng2) * (car.LatLng.lng - lng2);
        //b = Math.sqrt(b);
        var b = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(car.LatLng), new google.maps.LatLng(lat2, lng2));

        if (b < d) {
            tt = true;
            for (var i = 0; i < pcar.length; i++) {
                if (pcar[i].b > b) {
                    pcar[i] = { b: b, lat: car.LatLng.lat, lng: car.LatLng.lng };
                    tt = false;
                    return;
                }
            }
            if (tt) {
                pcar.push({ b: b, lat: car.LatLng.lat, lng: car.LatLng.lng });
            }
        }
        return;
    });
}

function initMap() {
    var hcmus = { lat: 10.762695566012058, lng: 106.68256759643555 };
    geocoder = new google.maps.Geocoder();
    distance = new google.maps.DistanceMatrixService;

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: hcmus
        //,clickableIcons: false
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function () {
        if (isPicking) {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            if (marker) {
                marker.setMap(null);
                marker = null;
            }

            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }

                if (marker == null) {
                    marker = new google.maps.Marker({
                        map: map,
                        title: place.name,
                        position: place.geometry.location
                    });
                    map.panTo(place.geometry.location);
                    carnear();
                }
            });
        }
    });

    map.addListener('click', function (e) {
        if (isPicking) {
            if (marker) {
                marker.setPosition(e.latLng);
            } else {
                marker = new google.maps.Marker({
                    map: map,
                    position: e.latLng
                });
            }
            addressCode(e.latLng);
            carnear();

        }
    });
};

window.onbeforeunload = function (event) {

    if (isPicking) {
        isPicking = false;
        $.each(vm.orders, function (idx, value) {
            if (value.key == vm.currentOrder) {
                var updates = {};
                updates['/' + vm.currentOrder + '/Status'] = 0;
                updates['/' + vm.currentOrder + '/Locator'] = '';
                orderRef.update(updates);

                $('#pac-input').val('');
                $('#pac-output').val('');

                if (marker != null) {
                    marker.setMap(null);
                    marker = null;
                }
                if (mcar != null && mcar.length > 0) {
                    mcar.forEach(function (m) {
                        m.setMap(null);
                    });
                }
                mcar = [];

                //loadOrders();
                vm.orders = [];

                return false;
            }
        });
    }


    if (vm.suser != '') {
        firebase.database().ref('/Locator/' + vm.suser).update({ isLogin: false });
    }


    firebase.auth().signOut();
    event.returnValue = "No exit";
};
