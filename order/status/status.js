//GLOBAL VAR
var orderRef;
var carRef;
var cars = [];

var vm;
var map;

var marker;
var mcar = [];
var cisV;

var directionsService;
var directionsDisplay;
var distance;

vm = new Vue({
    el: '#app',
    data: {
        currentOrder: '',
        orders: [],
        status: ["Chưa định vị", "Đang định vị", "Đang tìm xe", "Đã có xe nhận", "Không có xe nhận", "Hoàn thành chuyến đi"]
    },
    filters: {
        showTime: function (value) {
            if (!value) return null;
            var d = new Date(value);
            return d.toLocaleString();
        }
    },
    methods: {
        selectOrder: function (key) {
            var self = this;

            if (key == self.currentOrder)
                return;

            hideinfo(self.currentOrder);
            self.currentOrder = key;
            showinfo(key);

        }
    }
});

$('#btnLogin').on('click', function () {
    if (!firebase.auth().currentUser) {
        var email = "user@nvb.us";
        var pass = $('#txtPass').val();

        //login
        firebase.auth().signInWithEmailAndPassword(email, pass)
            .catch(function (error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode === 'auth/wrong-password') {
                    alert('Mật khẩu sai.');
                } else {
                    alert(errorMessage);
                }
                console.log(error);
            });
    }
});

$('#btnLogout').on('click', function () {
    if (firebase.auth().currentUser) {
        firebase.auth().signOut();
        hideinfo(self.currentOrder);
    }
});

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        $('#modalLogin').modal('hide');
        loadOrders();
    } else {
        $('#modalLogin').modal('show');
    }
});

var loadOrders = function () {

    carRef = firebase.database().ref('/Car/');
    carRef.off();
    var getCar = function (data) {
        for (var ii = 0; ii < cars.length; ii++) {
            if (cars[ii].key == data.key) {
                cars[ii].val = data.val();
                hideinfo(vm.currentOrder);
                showinfo(vm.currentOrder);
                return;
            }
        }
        cars.push({ key: data.key, val: data.val() });
    };
    carRef.on("child_added", getCar);
    carRef.on("child_changed", getCar);

    orderRef = firebase.database().ref('Order');
    orderRef.off();

    var getOrder = function (data) {
        if (data.val().Status > 3) {
            $.each(vm.orders, function (idx, value) {
                if (value.key == data.key) {
                    vm.orders.splice(idx, 1);
                    hideinfo(value.key);
                    return false;
                }
            });
        } else {
            if (Date.now() - data.val().Time > 3600000){
                orderRef.child(data.key).update({Status: 4});
                return;
            }
            for (var i = 0; i < vm.orders.length; i++) {
                if (vm.orders[i].key == data.key) {
                    vm.orders[i].info = data.val();
                    hideinfo(data.key);
                    showinfo(data.key);
                    return;
                }
            }
            vm.orders.push({ key: data.key, info: data.val() });
        }
    }
    orderRef.limitToLast(50).on("child_added", getOrder);
    orderRef.limitToLast(50).on("child_changed", getOrder);
};

var typeofCar = { true: 'Xe VIP', false: 'Xe thường' };
function showinfo(key) {
    if (key == vm.currentOrder) {
        $.each(vm.orders, function (idxx, value) {
            if (value.key == key) {
                if (value.info.Status == 2) {
                    marker = new google.maps.Marker({
                        map: map,
                        position: value.info.LatLng
                    });
                    map.panTo(value.info.LatLng);
                    cisV = value.info.IsVIP;
                    carnear();
                }
                if (value.info.Status == 3) {
                    $.each(cars, function (idx, car) {
                        if (car.key == value.info.CarID) {
                            directionsService.route({
                                origin: car.val.LatLng,
                                destination: value.info.LatLng,
                                travelMode: 'DRIVING'
                            }, function (response, status) {
                                if (status === 'OK') {
                                    console.log(response);
                                    directionsDisplay.setDirections(response);
                                    marker = new google.maps.Marker({
                                        map: map,
                                        position: value.info.LatLng
                                    });

                                    var mc = new google.maps.Marker({
                                        map: map,
                                        position: car.val.LatLng,
                                        icon: "http://maps.google.com/mapfiles/kml/pal4/icon62.png"
                                    });

                                    var infowindow = new google.maps.InfoWindow();
                                    infowindow.setContent('<h4 class="text-center">'
                                        + car.key + '</h4><p>Số điện thoại: <b>'
                                        + car.val.Number + '</b></p><p>Tài xế: <b>'
                                        + car.val.DriverName + '</b></p><p>Biển số xe: <b>'
                                        + car.val.CarNumber + '</b></p><p>Loại xe: <b>'
                                        + typeofCar[car.val.IsVIP] + '</b></p>'
                                    );
                                    mc.addListener('click', function () {
                                        infowindow.open(map, mc);
                                    });
                                    mcar.push(mc);
                                }
                            });
                            return false;
                        }
                    });
                }
                return false;
            }
        });
    }
}

function hideinfo(key) {
    if (key == vm.currentOrder) {
        if (marker) {
            marker.setMap(null);
            marker = null;
        }
        mcar.forEach(function (m) {
            m.setMap(null);
        });
        mcar = [];
        pcar = [];

        directionsDisplay.setDirections({ routes: [] });
    }
}

function carnear() {
    findPcar(6000);
    mcar.forEach(function (m) {
        m.setMap(null);
    });
    mcar = [];
    console.log(pcar);
    pcar = pcar.slice(0, 25);

    distance.getDistanceMatrix({
        origins: pcar,
        destinations: [marker.position],
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
    }, function (response, status) {
        if (status !== 'OK') {
            alert('Error was: ' + status);
            return;
        }
        var n = response.rows.length;

        if (pcar.length != n)
        return;

        for (var i = 0; i < n; i++) {
            pcar[i].ct = pcar[i].ct + '</b></p><p>Khoảng cách: <b>' + response.rows[i].elements[0].distance.text + '</b></p>';
            pcar[i].distance = response.rows[i].elements[0].distance.value;
        }

        for (var i = 0; i < n - 1; i++) {
            for (var j = i + 1; j < n; j++) {
                if (response.rows[i].elements[0].distance.value > response.rows[j].elements[0].distance.value) {
                    var a = response.rows[i].elements[0];
                    response.rows[i].elements[0] = response.rows[j].elements[0];
                    response.rows[j].elements[0] = a;
                    var b = pcar[i];
                    pcar[i] = pcar[j];
                    pcar[j] = b;
                }
            }
        }
        pcar = pcar.slice(0, 10);

        console.log(pcar);

        pcar.forEach(function (car) {
            if (car.distance > 6000)
                return false;
            var mc = new google.maps.Marker({
                map: map,
                position: { lat: car.lat, lng: car.lng },
                icon: "http://maps.google.com/mapfiles/kml/pal4/icon62.png"
            });
            var infow = new google.maps.InfoWindow();
            infow.setContent(car.ct);
            mc.addListener('click', function () {
                infow.open(map, mc);
            });
            mcar.push(mc);
        });
        pcar = [];
    });
};
var pcar = [];
var tt;
function findPcar(d) {
    var lat2 = marker.position.lat(), lng2 = marker.position.lng();
    $.each(cars, function (idx, car) {
        if (car.val.hasPicked || car.val.IsVIP != cisV) {
            return;
        }
        var b = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(car.val.LatLng), new google.maps.LatLng(lat2, lng2));
        if (b < d) {
            tt = true;
            for (var i = 0; i < pcar.length; i++) {
                if (pcar[i].b > b) {
                    var ct = '<h4 class="text-center">'
                        + car.key + '</h4><p>Số điện thoại: <b>'
                        + car.val.Number + '</b></p><p>Lái xe: <b>'
                        + car.val.DriverName + '</b></p><p>Biển số xe: <b>'
                        + car.val.CarNumber + '</b></p><p>Loại xe: <b>'
                        + typeofCar[car.val.IsVIP] + '</b></p>';
                    pcar[i] = { b: b, lat: car.val.LatLng.lat, lng: car.val.LatLng.lng, ct: ct };
                    tt = false;
                    return;
                }
            }
            if (tt) {
                var ct = '<h4 class="text-center">'
                    + car.key + '</h4><p>Số điện thoại: <b>'
                    + car.val.Number + '</b></p><p>Lái xe: <b>'
                    + car.val.DriverName + '</b></p><p>Biển số xe: <b>'
                    + car.val.CarNumber + '</b></p><p>Loại xe: <b>'
                    + typeofCar[car.val.IsVIP] + '</b></p>';
                pcar.push({ b: b, lat: car.val.LatLng.lat, lng: car.val.LatLng.lng, ct: ct });
            }
        }
        return;
    });
}

function initMap() {
    directionsService = new google.maps.DirectionsService;
    var hcmus = { lat: 10.762695566012058, lng: 106.68256759643555 };

    distance = new google.maps.DistanceMatrixService;

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: hcmus,
        clickableIcons: false
    });
    var rendererOptions = {
        map: map,
        suppressMarkers: true,
        polylineOptions: {
            strokeColor: "#759fdd",
            strokeWeight: 7
        }
    }
    directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
    directionsDisplay.setPanel(document.getElementById('driPanel'));
};

function checkTime(){
    $.each(vm.orders, function (idx, value) {
        if (Date.now() - value.info.Time > 3600000) {
            orderRef.child(value.key).update({Status: 4});
        }
    });
    setInterval("checkTime()", 1800000);
}

setInterval("checkTime()", 1800000);
